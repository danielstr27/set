# Set Game
The Set Game is a simple automatic game, that shuffles for you a deck(dozen) of cards.
After that it tries to brute force find some set(trio of cards) that  each feature is either the same on all cards or different on all cards.

###Installation instructions
For your ease, i've uploaded the jar too, and a batch file so you could run the assignment(in contrast to the convention, to not upload executable and build byproducts).
If you would like to build the project just run gradle build in the project directory.

### Notes
######Complexity time of finding a set in the program is 12^3 which is roughly O(1)
I wanted to enable configuration-based loading, in a lazy(the current implementation) and
eager mode. Decided to stick only with lazy, as it's an overkill probably. 
But if I would implement eager option, I would basically create all of the 3^4 possible card permutations,
and randomize the order of the cards. Then when requested, to iterate through the collection(even arraylist would've been great here ).
Lazy is generally less efficient, but I though if i would explain it to you + fast imlementation
you would accept it. 

###### I know its not good practice to break the hashcode-equals contract, but the basic implementation of equals is good enough for us.
