package game;

import java.util.ArrayList;

import game.entities.Card;
import game.entities.Set;
import game.enums.Color;
import game.enums.Number;
import game.enums.Shading;
import game.enums.Symbol;

public class GameHandler {

    public Set findSet(ArrayList<Card> deck) {

        for (int firstCardIndex = 0; firstCardIndex < deck.size(); firstCardIndex++) {
            for (int secondCardIndex = firstCardIndex + 1; secondCardIndex < deck.size(); secondCardIndex++) {
                for (int thirdCardIndex = secondCardIndex + 1; thirdCardIndex < deck.size(); thirdCardIndex++) {
                    if (isSet(deck.get(firstCardIndex), deck.get(secondCardIndex), deck.get(thirdCardIndex))) {
                        return new Set(deck.get(firstCardIndex), deck.get(secondCardIndex), deck.get(thirdCardIndex));
                        
                    }
                }
            }
        }

        return null;
    }

	public ArrayList<Card> createDeck(int size) {
		ArrayList<Card> cards = new ArrayList<>();
        for(int cardIndex = 0;cardIndex < size; cardIndex++)
        {
            cards.add(CardFactory.getCard());
        }

        return cards;
	}

    private boolean isSet(Card card, Card card2, Card card3) {
        boolean colorValid = isColorVaild(card.getColor(), card2.getColor(), card3.getColor());
        boolean symbolValid = isSymbolVaild(card.getSymbol(), card2.getSymbol(), card3.getSymbol());
        boolean numberValid = isNumberVaild(card.getNumber(), card2.getNumber(), card3.getNumber());
        boolean shadingValid = isShadingVaild(card.getShading(), card2.getShading(), card3.getShading());

        return colorValid && symbolValid && numberValid && shadingValid;
    }

    private boolean isShadingVaild(Shading shading, Shading shading2, Shading shading3) {
        if (shading == shading2 && shading == shading3) {
            return true;
        } else if (shading != shading2 && shading != shading3 && shading2 != shading3) {
            return true;
        }

        return false;
    }

    private boolean isNumberVaild(Number number, Number number2, Number number3) {
        if (number == number2 && number == number3) {
            return true;
        } else if (number != number2 && number != number3 && number2 != number3) {
            return true;
        }

        return false;
    }

    private boolean isSymbolVaild(Symbol symbol, Symbol symbol2, Symbol symbol3) {
        if (symbol == symbol2 && symbol == symbol3) {
            return true;
        } else if (symbol != symbol2 && symbol != symbol3 && symbol2 != symbol3) {
            return true;
        }

        return false;
    }

    private boolean isColorVaild(Color color, Color color2, Color color3) {
        if (color == color2 && color == color3) {
            return true;
        } else if (color != color2 && color != color3 && color2 != color3) {
            return true;
        }

        return false;
    }

}
