package game.enums;

import lombok.Getter;

@Getter
public enum Shading {
    SOLID('#'),
    STRIPPED('/'),
    OPEN('_');

    private final char value;
    
    private Shading(char value)
    {
        this.value = value;
    }
}
