package game.enums;

import lombok.Getter;

@Getter
public enum Color {
    RED('R'), GREEN('G'), PURPLE('P');

    private final char value;
    
    private Color(char value)
    {
        this.value = value;
    }
}
