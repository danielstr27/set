package game.enums;

import lombok.Getter;

@Getter
public enum Number {
    ONE(1),TWO(2), THREE(3);
    private final int value;
    private Number(int value)
    {
        this.value = value;
    }
}
