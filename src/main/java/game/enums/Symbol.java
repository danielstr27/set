package game.enums;

import lombok.Getter;

@Getter
public enum Symbol {
    DIAMOND('V'),
    SQUIGGLE('Z'),
    OVAL('O');
    
    private final char value;
    private Symbol(char value)
    {
        this.value = value;
    }
}
