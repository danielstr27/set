package game;

import java.util.ArrayList;

import game.entities.Card;
import game.entities.Set;

public class Main {
    private static final int DECK_SIZE = 12;
    private static final int SET_SIZE = 3;

    public static void main(String[] args) {
        Printer.printBanner();

        GameHandler handler = new GameHandler();

        ArrayList<Card> deck = handler.createDeck(DECK_SIZE);
        for (int index = 0; index < DECK_SIZE / SET_SIZE; index++) {
            Set set = handler.findSet(deck);
            if(set == null)
            {
                break;
            }
            
            System.out.println("Set Found:");
            Printer.printSet(set);

            removeSetFromDeck(set,deck);
        }
        
        if(!deck.isEmpty())
        {
            System.out.println("Remaining Cards:");
            Printer.printCards(deck);
        }

        Printer.printEndGame();
    }

    private static void removeSetFromDeck(Set set, ArrayList<Card> deck) {
        deck.remove(set.getFirstCard());
        deck.remove(set.getSecondCard());
        deck.remove(set.getThirdCard());
    }
}
