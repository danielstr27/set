package game.entities;

import game.Printer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Set {
    private Card firstCard;
    private Card secondCard;
    private Card thirdCard;

    @Override
    public String toString() {
        return Printer.printTrio(firstCard, secondCard, thirdCard);
    }
}
