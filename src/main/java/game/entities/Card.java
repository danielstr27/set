package game.entities;

import game.enums.Color;
import game.enums.Number;
import game.enums.Shading;
import game.enums.Symbol;
import lombok.Data;
import lombok.Setter;

@Data
public class Card {
    private Number number;
    private Symbol symbol;
    private Shading shading;
    private Color color;

    // Trinary base unique representation. Card "code"
    @Override
    public int hashCode() {
        return number.ordinal() + symbol.ordinal() * 3 + shading.ordinal() * 9 + color.ordinal() * 27;
    }

    @Override
    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(number.getValue());
        buffer.append(symbol.getValue());
        buffer.append(shading.getValue());
        buffer.append(color.getValue());

        return buffer.toString();
    }
}
