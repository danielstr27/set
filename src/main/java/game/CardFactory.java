package game;

import game.entities.Card;
import game.enums.Color;
import game.enums.Number;
import game.enums.Symbol;
import game.enums.Shading;
import java.util.HashSet;

public class CardFactory {
    private static HashSet<Card> createdCards = new HashSet<>();

    public static Card getCard() {
        if(createdCards.size() == 81)
        {
            return createdCards.stream().findAny().get();
        }

        Card card = randomCard();
        while(createdCards.contains(card))
        {
            card = randomCard();
        }

        createdCards.add(card);
        return card;
    }

    private static Card randomCard()
    {
        Card card = new Card();
        
        int colorIndex = (int)(Math.random() * ((Color.values().length)));
        card.setColor(Color.values()[colorIndex]);
        
        int numberIndex = (int)(Math.random() * ((Number.values().length)));
        card.setNumber(Number.values()[numberIndex]);
        
        int symbolIndex = (int)(Math.random() * ((Symbol.values().length)));
        card.setSymbol(Symbol.values()[symbolIndex]);

        int shadingIndex = (int)(Math.random() * ((Shading.values().length)));
        card.setShading(Shading.values()[shadingIndex]);

        return card;
    }
}
