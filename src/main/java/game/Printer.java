package game;

import java.util.ArrayList;

import game.entities.Card;
import game.entities.Set;

public class Printer {
    private static final String SEPERATOR_STRING = " | ";

    public static void printSet(Set set) {
        System.out.println(set);
    }

    public static void printCards(ArrayList<Card> cards) {
        for (int numberPrinted = 0; numberPrinted < cards.size(); numberPrinted += 3) {
            System.out.println(printTrio(cards.get(numberPrinted), cards.get(numberPrinted + 1), cards.get(numberPrinted + 2)));
        }
    }

    public static String printTrio(Card card, Card card2, Card card3) {
        StringBuffer buffer = new StringBuffer();

        buffer.append(card);
        buffer.append(SEPERATOR_STRING);
        buffer.append(card2);
        buffer.append(SEPERATOR_STRING);
        buffer.append(card3);

        return buffer.toString();
    }

    public static void printEndGame() {
        System.out.println(".------..------..------.     .------..------..------..------.");
        System.out.println("|E.--. ||N.--. ||D.--. |.-.  |G.--. ||A.--. ||M.--. ||E.--. |");
        System.out.println("| (\\/) || :(): || :/\\: ((5)) | :/\\: || (\\/) || (\\/) || (\\/) |");
        System.out.println("| :\\/: || ()() || (__) |'-.-.| :\\/: || :\\/: || :\\/: || :\\/: |");
        System.out.println("| '--'E|| '--'N|| '--'D| ((1)) '--'G|| '--'A|| '--'M|| '--'E|");
        System.out.println("`------'`------'`------'  '-'`------'`------'`------'`------'");
    }

    public static void printBanner() {
        System.out.println(".------..------..------.     .------..------..------..------.");
        System.out.println("|S.--. ||E.--. ||T.--. |.-.  |G.--. ||A.--. ||M.--. ||E.--. |");
        System.out.println("| :/\\: || (\\/) || :/\\: ((5)) | :/\\: || (\\/) || (\\/) || (\\/) |");
        System.out.println("| :\\/: || :\\/: || (__) |'-.-.| :\\/: || :\\/: || :\\/: || :\\/: |");
        System.out.println("| '--'S|| '--'E|| '--'T| ((1)) '--'G|| '--'A|| '--'M|| '--'E|");
        System.out.println("`------'`------'`------'  '-'`------'`------'`------'`------'");
    }

}
