import game.CardFactory;
import game.GameHandler;
import game.entities.Card;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    @Test
    @DisplayName("No duplicates when cards created")
    public void givenMultipleCardsThenMustBeNoDuplicates() {
        HashSet<Card> cardsCreated = new HashSet<>();
        Card card;

        final int CARD_LIMIT = 81;
        int numberOfCardsCreated = 0;
        while (numberOfCardsCreated < CARD_LIMIT) {
            card = CardFactory.getCard();
            assertFalse(cardsCreated.contains(card));
            cardsCreated.add(card);
            numberOfCardsCreated++;
        }
    }

    @Test
    @DisplayName("Validate deck with 3 cards that has set in it")
    public void givenExistingValidSetInDeckThenSetIsReturned() {
        GameHandler handler = new GameHandler();
        ArrayList<Card> deck = TestUtils.deckWithValidSet();
        assertNotNull(handler.findSet(deck));
    }

    @Test
    @DisplayName("Validate deck with dozen cards")
    public void givenDeckByGameHandlerThenHeHasTwelveElements() {
        GameHandler handler = new GameHandler();
        assertEquals(handler.createDeck(12).size(), 12);
    }

    @Test
    @DisplayName("Validate deck with 3 cards that doesn't meet the set criteria")
    public void givenDeckWithNoValidSetThenNullIsReturned() {
        GameHandler handler = new GameHandler();
        ArrayList<Card> deck = TestUtils.deckWithNoValidSet();
        assertNull(handler.findSet(deck));
    }
}
