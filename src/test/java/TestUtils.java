import java.util.ArrayList;

import game.entities.*;
import game.enums.Color;
import game.enums.Number;
import game.enums.Shading;
import game.enums.Symbol;

public class TestUtils {

    public static ArrayList<Card> deckWithValidSet()
    {
        ArrayList<Card> arrayList = new ArrayList<>();
        Card card = new Card();
        Card card2 = new Card();
        Card card3 = new Card();

        card.setColor(Color.GREEN);
        card.setShading(Shading.OPEN);
        card.setNumber(Number.ONE);
        card.setSymbol(Symbol.DIAMOND);
        arrayList.add(card);
        
        card2.setColor(Color.GREEN);
        card2.setShading(Shading.SOLID);
        card2.setNumber(Number.TWO);
        card2.setSymbol(Symbol.DIAMOND);  
        arrayList.add(card2);
        
        card3.setColor(Color.GREEN);
        card3.setShading(Shading.STRIPPED);
        card3.setNumber(Number.THREE);
        card3.setSymbol(Symbol.DIAMOND);
        arrayList.add(card3);
        
        return arrayList;
    }

    public static ArrayList<Card> deckWithNoValidSet()
    {
        ArrayList<Card> arrayList = new ArrayList<>();
        Card card = new Card();
        Card card2 = new Card();
        Card card3 = new Card();

        card.setColor(Color.RED);
        card.setShading(Shading.OPEN);
        card.setNumber(Number.ONE);
        card.setSymbol(Symbol.DIAMOND);
        arrayList.add(card);
        
        card2.setColor(Color.GREEN);
        card2.setShading(Shading.SOLID);
        card2.setNumber(Number.TWO);
        card2.setSymbol(Symbol.OVAL);  
        arrayList.add(card2);
        
        card3.setColor(Color.GREEN);
        card3.setShading(Shading.STRIPPED);
        card3.setNumber(Number.THREE);
        card3.setSymbol(Symbol.DIAMOND);
        arrayList.add(card3);
        
        return arrayList;
    }
}
